# Simple Rust Card Game

This is a simple card game using Rust that is intended to show a basic understanding of the Rust language. 

# The Game
The game takes place on a 3 by 3 grid and each player has a hand of 5 cards. The players take turns placing their cards anywhere on a non occupied space on the grid. Each card has 4 numbers on them; this is the power of each side. When a card is placed, it "attacks" or "invades" each adjacent tile. If the number is greater than the adjacent tile, then ownership of that card is taken by the attacker. The game ends when the board is completely filled and a winner is declared from the player with the most owned tiles. 