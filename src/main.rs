use rand::Rng;
use std::io;
use std::vec;

#[derive(Copy, Clone, PartialEq)]
enum Owner {
    Player,
    Ai,
}

#[derive(Copy, Clone)]
struct Board {
    pub cards: [[Option<Card>; 3]; 3],
    space: u32,
}

impl Board {
    pub fn new() -> Self {
        Board {
            cards: [[None; 3]; 3],
            space: 9,
        }
    }

    pub fn place_card(&mut self, pos: (usize, usize), card: Card) -> Result<i32, String> {
        if self.cards[pos.1][pos.0].is_some() {
            Err(format!("The space at {:?} is filled!", pos))
        } else {
            self.cards[pos.1][pos.0] = Some(card);
            let mut won = 0;
            if pos.0 > 0 {
                if let Some(opponent) = &mut self.cards[pos.1][pos.0 - 1] {
                    if card.left > opponent.right {
                        opponent.owner = card.owner;
                        won += 1;
                    }
                }
            }
            if pos.0 < 2 {
                if let Some(opponent) = &mut self.cards[pos.1][pos.0 + 1] {
                    if card.right > opponent.left {
                        opponent.owner = card.owner;
                        won += 1;
                    }
                }
            }
            if pos.1 > 0 {
                if let Some(opponent) = &mut self.cards[pos.1 - 1][pos.0] {
                    if card.top > opponent.bottom {
                        opponent.owner = card.owner;
                        won += 1;
                    }
                }
            }
            if pos.1 < 2 {
                if let Some(opponent) = &mut self.cards[pos.1 + 1][pos.0] {
                    if card.bottom > opponent.top {
                        opponent.owner = card.owner;
                        won += 1;
                    }
                }
            }
            self.space -= 1;
            Ok(won)
        }
    }
}

struct Hand {
    cards: Vec<Card>,
}

impl Hand {
    pub fn new(owner: Owner) -> Self {
        let mut cards = Vec::new();
        for _ in 0..5 {
            cards.push(Card::new(owner))
        }
        Hand { cards }
    }

    pub fn player() -> Self {
        Self::new(Owner::Player)
    }

    pub fn ai() -> Self {
        Self::new(Owner::Ai)
    }
}

/// The data structure for a Card
#[derive(Copy, Clone)]
struct Card {
    top: isize,
    bottom: isize,
    left: isize,
    right: isize,
    pub owner: Owner,
}

impl Card {
    pub fn new(owner: Owner) -> Self {
        Card {
            top: rand::thread_rng().gen_range(1, 10),
            bottom: rand::thread_rng().gen_range(1, 10),
            left: rand::thread_rng().gen_range(1, 10),
            right: rand::thread_rng().gen_range(1, 10),
            owner: owner,
        }
    }

    pub fn player() -> Self {
        Self::new(Owner::Player)
    }

    pub fn ai() -> Self {
        Self::new(Owner::Ai)
    }

    pub fn reset_card(&mut self, owner: Owner) -> () {
        self.top = rand::thread_rng().gen_range(1, 10);
        self.bottom = rand::thread_rng().gen_range(1, 10);
        self.left = rand::thread_rng().gen_range(1, 10);
        self.right = rand::thread_rng().gen_range(1, 10);
        self.owner = owner;
    }
}

impl std::fmt::Display for Owner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Player => "x",
                Self::Ai => "o",
            }
        )
    }
}

impl std::fmt::Display for Hand {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.cards.len() == 0 {
            return write!(f, "");
        }
        let mut lines = vec![String::new(); 5];
        for (i, card) in self.cards.iter().enumerate() {
            if i > 0 {
                lines[0] += " ";
                lines[1] += " ";
                lines[2] += " ";
                lines[3] += " ";
                lines[4] += " ";
            }
            lines[0] += "|---------|";
            lines[1] += &format!("|    {}    |", card.top);
            lines[2] += &format!("| {}  {}  {} |", card.left, card.owner, card.right);
            lines[3] += &format!("|    {}    |", card.bottom);
            lines[4] += &format!("|---------|");
        }
        let mut res = String::new();
        for l in lines.iter() {
            res += l;
            res += "\n";
        }
        write!(f, "{}", res)
    }
}

impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut res = String::new();
        for row in self.cards.iter() {
            let mut lines = vec![String::new(); 5];
            for (i, card) in row.iter().enumerate() {
                if let Some(card) = card {
                    if i > 0 {
                        lines[0] += " ";
                        lines[1] += " ";
                        lines[2] += " ";
                        lines[3] += " ";
                        lines[4] += " ";
                    }
                    lines[0] += "|---------|";
                    lines[1] += &format!("|    {}    |", card.top);
                    lines[2] += &format!("| {}  {}  {} |", card.left, card.owner, card.right);
                    lines[3] += &format!("|    {}    |", card.bottom);
                    lines[4] += &format!("|---------|");
                } else {
                    if i > 0 {
                        lines[0] += " ";
                        lines[1] += " ";
                        lines[2] += " ";
                        lines[3] += " ";
                        lines[4] += " ";
                    }
                    lines[0] += "|---------|";
                    lines[1] += &format!("|         |");
                    lines[2] += &format!("|         |");
                    lines[3] += &format!("|         |");
                    lines[4] += &format!("|---------|");
                }
            }
            for l in lines.iter() {
                res += l;
                res += "\n";
            }
        }
        write!(f, "{}", res)
    }
}

fn clear_terminal() {
    print!("\x1B[2J\x1B[1;1H");
}

fn main() -> () {
    // Format of the cards and board example:
    // |---------|---------|---------|
    // |    1    |    5    |    9    |
    // | 3  x  7 | 3  o  4 | 5  o  3 |
    // |    2    |    7    |    1    |
    // |---------|---------|---------|
    // |         |    5    |         |
    // |         | 3  x  4 |         |
    // |         |    7    |         |
    // |---------|---------|---------|
    // |    1    |         |    9    |
    // | 3  x  7 |         | 5  o  3 |
    // |    2    |         |    1    |
    // |---------|---------|---------|

    let mut board = Board::new();
    let mut player_hand = Hand::player();
    let mut ai_hand = Hand::ai();

    let mut player_turn = true;
    loop {
        clear_terminal();
        if board.space > 0 {
            println!(
                "{}",
                if player_turn {
                    "Your Turn!"
                } else {
                    "Thinking..."
                }
            );
            println!("{}", board);
            println!("Your hand:");
            println!("{}", player_hand);
            if player_turn {
                loop {
                    println!("Choose a card and position (c, x, y ex. 0, 0, 1):");
                    let mut buf = String::new();
                    io::stdin().read_line(&mut buf);
                    let input: Vec<Result<usize, _>> =
                        buf.trim().split(",").map(|v| v.trim().parse()).collect();
                    if input.iter().any(|v| v.is_err() || input.len() != 3) {
                        println!("Invalid input!");
                    } else {
                        let input: Vec<usize> =
                            input.iter().map(|v| *v.as_ref().unwrap()).collect();
                        if input[0] > player_hand.cards.len() {
                            println!("Card index out of range!");
                        } else {
                            match board
                                .place_card((input[1], input[2]), player_hand.cards[input[0]])
                            {
                                Err(msg) => {
                                    println!("{}", msg);
                                    continue;
                                }
                                _ => {
                                    player_hand.cards.remove(input[0]);
                                    break;
                                }
                            };
                        }
                    }
                }
            } else {
                let mut best = -1;
                let mut best_move = None;
                let mut card_ind = 0;
                for (k, &card) in ai_hand.cards.iter().enumerate() {
                    for (j, row) in board.cards.iter().enumerate() {
                        for (i, space) in row.iter().enumerate() {
                            if space.is_none() {
                                let mut ai_board = board.clone();
                                let score = ai_board.place_card((i, j), card).unwrap();
                                if score > best {
                                    best = score;
                                    best_move = Some((card, i, j));
                                    card_ind = k;
                                }
                            }
                        }
                    }
                }
                if let Some((c, i, j)) = best_move {
                    board.place_card((i, j), c);
                    ai_hand.cards.remove(card_ind);
                }
            }
        } else {
            let mut player_score = 0;
            let mut ai_score = 0;
            for row in board.cards.iter() {
                for card in row.iter() {
                    match card.unwrap().owner {
                        Owner::Player => player_score += 1,
                        Owner::Ai => ai_score += 1,
                    };
                }
            }
            println!(
                "Game over! {}",
                if player_score > ai_score {
                    "You won!"
                } else {
                    "You lost!"
                }
            );
            println!("Play again? (y/n)");
            let mut buf = String::new();
            io::stdin().read_line(&mut buf);
            if buf.trim().to_lowercase() == "y" {
                board = Board::new();
                player_hand = Hand::player();
                ai_hand = Hand::ai();
                player_turn = player_score < ai_score;
                continue;
            } else {
                break;
            }
        }
        player_turn = !player_turn;
    }
}
